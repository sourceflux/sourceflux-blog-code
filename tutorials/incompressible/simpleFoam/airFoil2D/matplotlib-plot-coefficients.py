#!/usr/bin/python

from matplotlib import pyplot as plt
import numpy as np
plt.rc('text', usetex=True)

coeffData = np.loadtxt('postProcessing/forceCoefficients/0/forceCoeffs.dat', skiprows=1)

plt.plot(coeffData[:,0], coeffData[:,1], linewidth=2, color='black', label="$C_m$")
plt.plot(coeffData[:,0], coeffData[:,2], linewidth=2, color='blue', label="$C_d$")
plt.plot(coeffData[:,0], coeffData[:,3], linewidth=2, color='magenta', label="$C_l$")
plt.tick_params(axis='both', which='major', labelsize=16)
plt.ylabel('Force coefficients', fontsize=20)
plt.xlabel('Iteration count', fontsize=20)
plt.legend()
plt.savefig("force-coefficients-plot.pdf")
